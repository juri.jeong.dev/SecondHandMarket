//
//  ContentView.swift
//  SecondHandMarket
//
//  Created by 정주리 on 2022/03/31.
//

import SwiftUI

struct ContentView: View {
    
    var body: some View {
        MainTabView()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
