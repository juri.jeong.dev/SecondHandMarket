//
//  HomeView.swift
//  SecondHandMarket
//
//  Created by 정주리 on 2022/03/31.
//

import SwiftUI

struct HomeView: View {
    var body: some View {
        Text("Home View")
    }
}

struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView()
    }
}
