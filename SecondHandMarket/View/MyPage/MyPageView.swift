//
//  MyPageView.swift
//  SecondHandMarket
//
//  Created by 정주리 on 2022/03/31.
//

import SwiftUI

struct MyPageView: View {
    var body: some View {
        Text("My Page View")
    }
}

struct MyPageView_Previews: PreviewProvider {
    static var previews: some View {
        MyPageView()
    }
}
