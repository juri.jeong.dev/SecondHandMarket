//
//  MainTabView.swift
//  SecondHandMarket
//
//  Created by 정주리 on 2022/03/31.
//

import SwiftUI

struct MainTabView: View {
    @State private var selectedIndex = 0
    
    var body: some View {
        NavigationView {
            TabView(selection: $selectedIndex) {
                HomeView()
                    .onTapGesture {
                        print("selected Index is : \(selectedIndex)")
                        selectedIndex = 0
                    }
                    .tabItem({
                        Image(systemName: "house")
                        Text("Home")
                    })
                    .tag(0)
                
                ChatView()
                    .onTapGesture {
                        print("selected Index is : \(selectedIndex)")
                        selectedIndex = 1
                    }
                    .tabItem({
                        Image(systemName: "bubble.left.and.bubble.right")
                        Text("Chats")
                    })
                    .tag(1)
                
                MyPageView()
                    .onTapGesture {
                        print("selected Index is : \(selectedIndex)")
                        selectedIndex = 2
                    }
                    .tabItem({
                        Image(systemName: "person")
                        Text("My Page")
                    })
                    .tag(2)
            }
            .accentColor(.black)
            .onAppear() {
                if #available(iOS 15.0, *) {
                    let appearance = UITabBarAppearance()
                    UITabBar.appearance().scrollEdgeAppearance = appearance
                }
                UITabBar.appearance().backgroundColor = .white
            }
            .navigationTitle(tabTitle)
        }
    }
    
    var tabTitle: String {
        switch selectedIndex {
        case 0: return "Home"
        case 1: return "Chats"
        case 2: return "My Page"
        default:
            return ""
        }
    }
}

struct MainTabView_Previews: PreviewProvider {
    static var previews: some View {
        MainTabView()
    }
}
