//
//  SecondHandMarketApp.swift
//  SecondHandMarket
//
//  Created by 정주리 on 2022/03/31.
//

import SwiftUI

@main
struct SecondHandMarketApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
